import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import registerServiceWorker from './registerServiceWorker';
import{Form} from './components/Form';

ReactDOM.render(<Form />, document.getElementById('root'));
registerServiceWorker();
