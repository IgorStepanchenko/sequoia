import React, {Component} from 'react';

class Table extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <table>
                <tbody>
                    <tr>
                    <td>Domain:</td>
                    <td>request.net</td>
                </tr>
                <tr>
                    <td>Registrar:</td>
                    <td>DomainPeople, Inc.</td>
                </tr>
                <tr>
                    <td>Registration Date:</td>
                    <td>1995-05-24</td>
                </tr>
                </tbody>
            </table>
        )
    }
}

export default Table