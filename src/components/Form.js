import React, {Component} from 'react';
import Table from './Table';

// esLint-disable-next-line
class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {value: "none", response: "", clickTime: 0, newDiv: ""};
        this.changeValue = this.changeValue.bind(this);
        this.chooseContent = this.chooseContent.bind(this);
        this.insertLinkInInput = this.insertLinkInInput.bind(this);
        this.showText = this.showText.bind(this);
    }

    changeValue(event) {
        event.preventDefault();
        this.setState({
            value: this.mainInput.value,
            clickTime: Date.now(),
        });
        this.showText();
    }

    chooseContent() {
        let divToReturn;
        switch (this.state.value) {
            case "успешно" :
                divToReturn = <div className="success"><Table/></div>
            break

            case "ошибка" :
                divToReturn = <div className="error">Error</div>
            break

            case "подождите" :
                divToReturn = <div className="wait">Wait</div>
            break

            default:
                divToReturn = null;
        }

        this.setState({newDiv: divToReturn});

    };

    showText() {
        setInterval(() => Math.floor((Date.now() - this.state.clickTime) / 1000) === 5 ? this.chooseContent() : false, 1000);
    }

    insertLinkInInput(event) {
        event.preventDefault();
        this.setState({value: event.target.innerHTML});
        this.mainInput.value = event.target.innerHTML;
        console.log(event.target.innerHTML);
    }


    render() {
        return (
            <div className="main-cont">
                <form onSubmit={this.changeValue}>
                    <span className="form-title">Whois query:</span>
                    <div className="form-inline">
                        <input className="form-control main-input" ref={(input) => this.mainInput = input} type="text"/>
                        <button  type="submit" className="btn btn-primary btn-submit">Submit</button>
                        <button onClick={this.showText} className="btn btn-secondary">Secondary</button>
                    </div>
                    <ul className="links-list">
                        <li><a onClick={this.insertLinkInInput} className="links-list-link" href="">code.ua</a></li>
                        <li><a onClick={this.insertLinkInInput} className="links-list-link" href="">google.com</a></li>
                    </ul>

                    <div className="results">{this.state.newDiv}</div>
                </form>
            </div>
        )
    }
}

export {Form}